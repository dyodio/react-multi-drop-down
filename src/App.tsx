import { useState } from 'react';
import './App.css'
import { CustomizedMutliDropDown } from './components/CustomizedMultiDropDown/CustomizedMutliDropDown';

const App = () => {
  return (
    <div className="App">
      <div>Drop down List</div>
      <CustomizedMutliDropDown
        tree={{
          a: {
            children: {
              a1: {
                children: null
              },
              a2: {
                children: {
                  a21: {
                    children: {
                      a211: {
                        children: null,
                      }
                    }
                  },
                  a22: {
                    children: null
                  }
                }
              }
            }
          },
          b: {
            children: null
          },
          c: {
            children: {
              c1: {
                children: {
                  c11: {
                    children: {
                      c111: {
                        children: null,
                      }
                    }
                  },
                  c12: {
                    children: null
                  }
                }
              },
              c2: {
                children: {
                  c21: {
                    children: {
                      c211: {
                        children: null,
                      }
                    }
                  },
                  c22: {
                    children: null
                  }
                }
              }
            }
          }
        }}
      />
    </div>
  );
};

export default App
