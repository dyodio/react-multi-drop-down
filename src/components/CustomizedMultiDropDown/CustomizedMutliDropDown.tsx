import { createContext, useContext, useState } from "react";
import { Checkbox } from "../Checkbox";
import { MultiDropDown, MultiDropDownProps } from "../MultiDropDown";
import { get as _get } from "lodash";

const treeColors = {
  0: 'blue',
  1: 'red',
  2: 'green',
  3: 'purple',
  4: 'yellow',
  5: 'orange',
  6: 'grey',
  7: 'cyan',
  8: 'midnightblue',
};

export const VincentItemComponent: MultiDropDownProps['itemComponent'] = (props) => <span style={{
  cursor: props.hasChildren ? 'pointer' : 'initial',
}} onClick={props.onToggleChildren}>
  ELEMENT DE VINCENT C11 !!!! {props.hasChildren ? props.opened ? 'Close' : 'Show Children >' : ''}
</span>

const customizedMultiDropDownContext = createContext<{
  itemsSelectedDict: Record<string, boolean>,
  setItemsSelectedDict: React.Dispatch<React.SetStateAction<Record<string, boolean>>>
}>({
  itemsSelectedDict: {},
  setItemsSelectedDict: () => { },
});

export const ItemComponent: MultiDropDownProps['itemComponent'] = ({
  elementKey,
  hasChildren,
  opened,
  onToggleChildren,
  ancestorList
}) => {
  const { itemsSelectedDict, setItemsSelectedDict } = useContext(customizedMultiDropDownContext);
  const keyPath = `${ancestorList}.${elementKey}`;
  return (
    <div style={{ display: 'flex' }}>
      {
        elementKey === 'c11' ?
          <VincentItemComponent {...{ elementKey, hasChildren, opened, onToggleChildren, ancestorList }} />
          : <span onClick={onToggleChildren} style={{
            cursor: hasChildren ? 'pointer' : 'initial',
            color: treeColors[ancestorList.length as keyof typeof treeColors] || 'black',
          }}>
            -{'>'} {elementKey} {hasChildren ? opened ? 'Close' : 'Show Children >' : ''}
          </span>
      }
      <span style={{ marginLeft: '10px' }}>
        <Checkbox checked={!!_get(itemsSelectedDict, keyPath)} onClick={() => setItemsSelectedDict(old => ({
          ...old,
          [keyPath]: !_get(old, keyPath)
        }))} />
      </span>
    </div>
  );
};

export type CustomizedMutliDropDownProps = {
  tree: MultiDropDownProps['tree'],
}

export const CustomizedMutliDropDown = (props: CustomizedMutliDropDownProps) => {
  const [itemsSelectedDict, setItemsSelectedDict] = useState({});
  return <customizedMultiDropDownContext.Provider value={{
    itemsSelectedDict,
    setItemsSelectedDict
  }}>
    <div>Number of items selected : {Object.values(itemsSelectedDict).filter(e => e === true).length}</div>
    <MultiDropDown
      tree={props.tree}
      itemComponent={ItemComponent}
    />
  </customizedMultiDropDownContext.Provider>
}