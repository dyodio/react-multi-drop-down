import React, { useRef, useState } from "react";

export type Tree = {
  [k: string]: {
    children: Tree | null;
  };
};

export type MultiDropDownProps = {
  itemComponent: (props: {
    opened: boolean;
    ancestorList: string[];
    hasChildren: boolean;
    elementKey: string;
    onToggleChildren: () => void;
  }) => JSX.Element;
  tree: Tree;
  ancestorList?: string[];
  ulTag?: string;
  liTag?: string;
};

export const MultiDropDown = (props: MultiDropDownProps) => {
  const Ul = props.ulTag || 'ul' as any;
  const Li = props.liTag || 'li' as any;
  const initialStateRef = useRef(Object.fromEntries(Object.keys(props.tree).map(key => ([key, false]))));
  const [treeKeysOpened, setTreeKeysOpened] = useState(initialStateRef.current);
  return <Ul className="multi-drop-down">
    {Object.entries(props.tree).map(([key, val]) => (
      <Li key={key}>
        <props.itemComponent
          opened={!!treeKeysOpened[key]}
          hasChildren={props.tree[key]?.children !== null}
          ancestorList={props.ancestorList || []}
          elementKey={key}
          onToggleChildren={() => setTreeKeysOpened(old => ({
            ...old,
            [key]: !old[key],
          }))}
        />
        {(val.children !== null && treeKeysOpened[key]) &&
          <MultiDropDown
            itemComponent={props.itemComponent}
            tree={val.children}
            ancestorList={[...(props.ancestorList || []), key]}
          />}
      </Li>
    ))}
  </Ul>
}
