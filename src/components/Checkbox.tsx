import { useEffect } from "react";

export type CheckboxProps = {
  checked: boolean;
  onClick: () => void;
};

export const Checkbox = (props: CheckboxProps) => {
  useEffect(() => {
    return () => console.log('dismount checkbox');
  });
  return (
    <div
      onClick={props.onClick}
      style={{
        cursor: 'pointer',
        height: '20px',
        width: '20px',
        border: 'solid 2px black',
        backgroundColor: props.checked ? 'blue' : 'transparent',
      }} />
  )
};
